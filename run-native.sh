#!/bin/bash
./mvnw clean package install -Pnative -Dquarkus.native.container-build=true -Dquarkus.container-image.build=true
docker build -f src/main/docker/Dockerfile.native -t quarkus/d4g2021-back-native .
docker-compose -f docker-compose-native.yml down --remove-orphans
docker-compose -f docker-compose-native.yml up
