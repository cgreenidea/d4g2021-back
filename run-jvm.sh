#!/bin/bash
./mvnw clean package install
docker build -f src/main/docker/Dockerfile.jvm -t quarkus/d4g2021-back-jvm .
docker-compose -f docker-compose-jvm.yml down --remove-orphans
docker-compose -f docker-compose-jvm.yml up
