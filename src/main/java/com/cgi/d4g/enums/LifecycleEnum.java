package com.cgi.d4g.enums;

public enum LifecycleEnum {
    ACQUISITION(0), CONCEPTION(1), REALISATION(2), DEPLOIEMENT(3), ADMINISTRATION(4), UTILISATION(5), MAINTENANCE(6), REVALORISATION(7), FIN_DE_VIE(8), N_A(9);

    public final Integer order;

    LifecycleEnum(Integer order) {
        this.order = order;
    }
}
