package com.cgi.d4g.enums;

import java.util.Comparator;

public class FamilyEnumComparator implements Comparator<FamilyEnum> {
    @Override
    public int compare(FamilyEnum o1, FamilyEnum o2) {
        return o1.order.compareTo(o2.order); // this flips the order
    }

    @Override
    public boolean equals(Object obj) {
        return false;
    }
}
