package com.cgi.d4g.enums;

public enum FamilyEnum {
    STRATEGY(0), ARCHITECTURE(4), BACKEND(6), CONTENT(3), FRONTEND(5), SPECIFICATIONS(1), UX_UI(2), STAPLE(7);

    public final Integer order;

    FamilyEnum(Integer order) {
        this.order = order;
    }
}
