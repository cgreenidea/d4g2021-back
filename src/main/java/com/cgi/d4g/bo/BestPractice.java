package com.cgi.d4g.bo;

import com.cgi.d4g.enums.FamilyEnum;
import com.cgi.d4g.enums.LifecycleEnum;

public class BestPractice {

    private String id;
    private String criteria;
    private FamilyEnum family;
    private String justifications;
    private String kpi;
    private String xKpi;
    private String yKpi;
    private LifecycleEnum lifeCycle;
    private String recommendation;
    private Boolean staple;
    private Boolean category;

    public BestPractice() {
        // empty constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCriteria() {
        return criteria;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public FamilyEnum getFamily() {
        return family;
    }

    public void setFamily(FamilyEnum familyEnum) {
        this.family = familyEnum;
    }

    public String getJustifications() {
        return justifications;
    }

    public void setJustifications(String justifications) {
        this.justifications = justifications;
    }

    public String getKpi() {
        return kpi;
    }

    public void setKpi(String kpi) {
        this.kpi = kpi;
    }

    public String getxKpi() {
        return xKpi;
    }

    public void setxKpi(String xKpi) {
        this.xKpi = xKpi;
    }

    public String getyKpi() {
        return yKpi;
    }

    public void setyKpi(String yKpi) {
        this.yKpi = yKpi;
    }

    public LifecycleEnum getLifeCycle() {
        return lifeCycle;
    }

    public void setLifeCycle(LifecycleEnum lifeCycle) {
        this.lifeCycle = lifeCycle;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }

    public Boolean getStaple() {
        return staple;
    }

    public void setStaple(Boolean staple) {
        this.staple = staple;
    }

    public Boolean getCategory() {
        return category;
    }

    public void setCategory(Boolean category) {
        this.category = category;
    }


}
