package com.cgi.d4g.bo;

import com.cgi.d4g.enums.FamilyEnum;

import java.util.List;

public class FamilyBpsResult {

    private FamilyEnum family;
    private List<BestPractice> bps;

    public FamilyBpsResult(FamilyEnum family, List<BestPractice> bps) {
        this.family = family;
        this.bps = bps;
    }

    public FamilyEnum getFamily() {
        return family;
    }

    public void setFamily(FamilyEnum family) {
        this.family = family;
    }

    public List<BestPractice> getBps() {
        return bps;
    }

    public void setBps(List<BestPractice> bps) {
        this.bps = bps;
    }
}
