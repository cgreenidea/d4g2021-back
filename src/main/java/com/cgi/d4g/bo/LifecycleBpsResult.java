package com.cgi.d4g.bo;

import com.cgi.d4g.enums.LifecycleEnum;

import java.util.LinkedList;
import java.util.List;

public class LifecycleBpsResult {
    private LifecycleEnum lifecycle;
    private List<FamilyBpsResult> familyBpsResult;

    public LifecycleEnum getLifecycle() {
        return lifecycle;
    }

    public void setLifecycle(LifecycleEnum lifecycle) {
        this.lifecycle = lifecycle;
    }

    public List<FamilyBpsResult> getFamilyBpsResult() {
        return familyBpsResult;
    }

    public void setFamilyBpsResult(List<FamilyBpsResult> familyBpsResult) {
        this.familyBpsResult = familyBpsResult;
    }

    public void addFamilyBpsResult(FamilyBpsResult familyBpsResult) {
        if (this.familyBpsResult == null) {
            this.familyBpsResult = new LinkedList<FamilyBpsResult>();
        }
        this.familyBpsResult.add(familyBpsResult);
    }
}
