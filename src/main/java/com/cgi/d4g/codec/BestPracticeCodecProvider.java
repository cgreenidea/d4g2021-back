package com.cgi.d4g.codec;

import com.cgi.d4g.bo.BestPractice;
import org.bson.codecs.Codec;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;

public class BestPracticeCodecProvider implements CodecProvider {

    @Override
    public <T> Codec<T> get(Class<T> aClass, CodecRegistry codecRegistry) {
        if (aClass == BestPractice.class) {
            return (Codec<T>) new BestPracticeCodec();
        }
        return null;
    }
}
