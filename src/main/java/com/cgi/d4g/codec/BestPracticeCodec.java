package com.cgi.d4g.codec;

import com.cgi.d4g.bo.BestPractice;
import com.cgi.d4g.enums.FamilyEnum;
import com.cgi.d4g.enums.LifecycleEnum;
import com.mongodb.MongoClientSettings;
import org.bson.BsonReader;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.bson.BsonWriter;
import org.bson.Document;
import org.bson.codecs.Codec;
import org.bson.codecs.CollectibleCodec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

import java.util.UUID;

public class BestPracticeCodec implements CollectibleCodec<BestPractice> {

    private final Codec<Document> documentCodec;

    public BestPracticeCodec() {
        this.documentCodec = MongoClientSettings.getDefaultCodecRegistry().get(Document.class);
    }

    @Override
    public BestPractice generateIdIfAbsentFromDocument(BestPractice bestPractice) {
        if (!documentHasId(bestPractice)) {
            bestPractice.setId(UUID.randomUUID().toString());
        }
        return bestPractice;
    }

    @Override
    public boolean documentHasId(BestPractice bestPractice) {
        return bestPractice.getId() != null;
    }

    @Override
    public BsonValue getDocumentId(BestPractice bestPractice) {
        return new BsonString(bestPractice.getId());
    }

    @Override
    public BestPractice decode(BsonReader bsonReader, DecoderContext decoderContext) {
        var document = documentCodec.decode(bsonReader, decoderContext);
        var bp = new BestPractice();

        bp.setId(document.getString("ID"));

        if (document.getBoolean("CATEGORY") != null) {
            bp.setCategory(document.getBoolean("CATEGORY"));
        }
        if (document.getString("CRITERIAS") != null) {
            bp.setCriteria(document.getString("CRITERIAS"));
        }

        bp.setFamily(FamilyEnum.valueOf(document.getString("FAMILY")));

        if (document.getString("JUSTIFICATIONS") != null) {
            bp.setJustifications(document.getString("JUSTIFICATIONS"));
        }
        if (document.getString("KPI") != null) {
            bp.setKpi(document.getString("KPI"));
        }

        bp.setRecommendation(document.getString("RECOMMENDATION"));

        if (document.getBoolean("STAPLE") != null) {
            bp.setStaple(document.getBoolean("STAPLE"));
        }
        if (document.getString("LIFECYCLE") != null) {
            bp.setLifeCycle(parseLifecycle(document.getString("LIFECYCLE")));
        }
        if (document.getString("XKPI") != null) {
            bp.setxKpi(document.getString("XKPI"));
        }
        if (document.getString("YKPI") != null) {
            bp.setyKpi(document.getString("YKPI"));
        }

        return bp;
    }

    @Override
    public void encode(BsonWriter bsonWriter, BestPractice bestPractice, EncoderContext encoderContext) {
        var doc = new Document();
        doc.put("ID", bestPractice.getId());
        doc.put("CATEGORY", bestPractice.getCategory());
        doc.put("CRITERIAS", bestPractice.getCriteria());
        doc.put("FAMILY", bestPractice.getFamily());
        doc.put("JUSTIFICATIONS", bestPractice.getJustifications());
        doc.put("KPI", bestPractice.getKpi());
        doc.put("LIFECYCLE", bestPractice.getLifeCycle());
        doc.put("RECOMMENDATION", bestPractice.getRecommendation());
        doc.put("STAPLE", bestPractice.getStaple());
        doc.put("XKPI", bestPractice.getxKpi());
        doc.put("YKPI", bestPractice.getyKpi());
        documentCodec.encode(bsonWriter, doc, encoderContext);
    }

    @Override
    public Class<BestPractice> getEncoderClass() {
        return BestPractice.class;
    }

    private LifecycleEnum parseLifecycle(String lifecycle) {
        switch (lifecycle) {
            case "Acquisition":
                return LifecycleEnum.ACQUISITION;
            case "Conception":
                return LifecycleEnum.CONCEPTION;
            case "Réalisation":
                return LifecycleEnum.REALISATION;
            case "Déploiement":
                return LifecycleEnum.DEPLOIEMENT;
            case "Administration":
                return LifecycleEnum.ADMINISTRATION;
            case "Utilisation":
                return LifecycleEnum.UTILISATION;
            case "Maintenance":
                return LifecycleEnum.MAINTENANCE;
            case "Revalorisation":
                return LifecycleEnum.REVALORISATION;
            case "Fin de Vie":
                return LifecycleEnum.FIN_DE_VIE;
            default:
                return LifecycleEnum.N_A;

        }
    }
}


