package com.cgi.d4g.service;

import com.cgi.d4g.bo.BestPractice;
import com.cgi.d4g.bo.FamilyBpsResult;
import com.cgi.d4g.bo.LifecycleBpsResult;
import com.cgi.d4g.enums.FamilyEnum;
import com.cgi.d4g.enums.LifecycleEnum;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class BestPracticeHelper {

    public BestPracticeHelper() {
    }

    public static Map<FamilyEnum, List<BestPractice>> getSortedBpMap(List<BestPractice> bps) {
        Map<FamilyEnum, List<BestPractice>> groupedMap = bps.stream()
            .filter(e -> (e.getStaple() == null || !e.getStaple()) && e.getJustifications() != null)
            .collect(Collectors.groupingBy(BestPractice::getFamily, Collectors.mapping(Function.identity(), Collectors.toList())));
        groupedMap.put(FamilyEnum.STAPLE, bps.stream().filter(e -> e.getStaple() != null && e.getStaple()).collect(Collectors.toList()));

        groupedMap = groupedMap.entrySet().stream().sorted(Comparator.comparing(o -> o.getKey().order)).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
            (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        return groupedMap;
    }

    public static List<LifecycleBpsResult> getReportBpMap(List<BestPractice> bps) {
        Map<LifecycleEnum, List<BestPractice>> groupedMap = bps.stream()
            .filter(e -> e.getJustifications() != null)
            .collect(Collectors.groupingBy(BestPractice::getLifeCycle, Collectors.mapping(Function.identity(), Collectors.toList())));
        List<LifecycleBpsResult> lifecycleBpsResultList = new ArrayList<>();
        for (Map.Entry<LifecycleEnum, List<BestPractice>> lifecycleEntry : groupedMap.entrySet()) {
            LifecycleBpsResult pendingResult = new LifecycleBpsResult();
            pendingResult.setLifecycle(lifecycleEntry.getKey());

            getSortedBpMap(lifecycleEntry.getValue()).forEach((key, value) -> pendingResult.addFamilyBpsResult(new FamilyBpsResult(key, value)));
            lifecycleBpsResultList.add(pendingResult);
        }
        lifecycleBpsResultList.sort(Comparator.comparing(o -> o.getLifecycle().order));
        return lifecycleBpsResultList;
    }

    public String encodeLifecycle(LifecycleEnum lc) {
        switch (lc.order) {
            case 0:
                return "Acquisition";
            case 1:
                return "Conception";
            case 2:
                return "Réalisation";
            case 3:
                return "Déploiement";
            case 4:
                return "Administration";
            case 5:
                return "Utilisation";
            case 6:
                return "Maintenance";
            case 7:
                return "Revalorisation";
            case 8:
                return "Fin de Vie";
            case 9:
                return "N/A";
            default:
                return "";
        }
    }

    public String encodeFamily(FamilyEnum f) {
        switch (f.order) {
            case 0:
                return "Stratégie";
            case 1:
                return "Spécifications";
            case 2:
                return "UX/UI";
            case 3:
                return "Contenu";
            case 4:
                return "Architecure";
            case 5:
                return "Front-end";
            case 6:
                return "Back-end";
            case 7:
                return "Incontournables";
            default:
                return "";
        }
    }
}
