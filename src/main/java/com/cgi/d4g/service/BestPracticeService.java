package com.cgi.d4g.service;

import com.cgi.d4g.bo.BestPractice;
import com.cgi.d4g.enums.FamilyEnum;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ApplicationScoped
public class BestPracticeService {

    @Inject
    MongoClient mongoClient;

    public List<BestPractice> list() {
        List<BestPractice> list = new ArrayList<>();
        MongoCursor<BestPractice> cursor = getCollection().find().iterator();
        collect(cursor, list);

        return list;
    }

    public List<BestPractice> findByFamily(FamilyEnum familyEnum) {
        List<BestPractice> list = new ArrayList<>();
        MongoCursor<BestPractice> cursor = getCollection().find(Filters.eq("FAMILY", familyEnum.toString())).iterator();
        collect(cursor, list);

        return list;
    }

    public List<BestPractice> findByIds(List<String> ids) {
        List<BestPractice> list = new ArrayList<>();
        MongoCursor<BestPractice> cursor = getCollection().find(Filters.in("ID", ids)).iterator();
        collect(cursor, list);
        return list;
    }

    public List<BestPractice> findCategoriesWithDetail() {
        List<BestPractice> list = new ArrayList<>();
        MongoCursor<BestPractice> cursor = getCollection().find(Filters.eq("CATEGORY", true)).iterator();
        collect(cursor, list);

        return list;
    }

    public List<Document> findCategories() {
        List<Document> list = new ArrayList<>();
        Bson match = Aggregates.match(Filters.eq("CATEGORY", true));
        Bson group = Aggregates.group("$FAMILY");
        mongoClient.getDatabase("d4g").getCollection("bp", Document.class).aggregate(Arrays.asList(group)).forEach(a -> list.add(a));
        return list;
    }

    private MongoCollection<BestPractice> getCollection() {
        return mongoClient.getDatabase("d4g").getCollection("bp", BestPractice.class);
    }

    private void collect(MongoCursor<BestPractice> cursor, List<BestPractice> list) {
        try (cursor) {
            while (cursor.hasNext()) {
                list.add(cursor.next());
            }
        }
    }
}
