package com.cgi.d4g.resources;

import com.cgi.d4g.bo.BestPractice;
import com.cgi.d4g.enums.FamilyEnum;
import com.cgi.d4g.service.BestPracticeHelper;
import com.cgi.d4g.service.BestPracticeService;
import io.quarkus.qute.CheckedTemplate;
import io.quarkus.qute.TemplateInstance;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Map;

@Path("selection")
public class SelectionResource {

    @Inject
    BestPracticeService bpService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String get() {
        return Templates.selection(BestPracticeHelper.getSortedBpMap(bpService.list()), new BestPracticeHelper()).render();
    }

    @CheckedTemplate
    public static class Templates {
        private Templates() {
        }

        public static native TemplateInstance selection(Map<FamilyEnum, List<BestPractice>> bps, BestPracticeHelper helper);
    }
}
