package com.cgi.d4g.resources;

import com.cgi.d4g.bo.BestPractice;
import com.cgi.d4g.enums.FamilyEnum;
import com.cgi.d4g.service.BestPracticeHelper;
import com.cgi.d4g.service.BestPracticeService;
import io.quarkus.qute.CheckedTemplate;
import io.quarkus.qute.TemplateInstance;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Map;

@Path("/cart")
public class CartResource {

    @Inject
    BestPracticeService bpService;

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public String get(@FormParam("checked") List<String> ids) {
        return CartResource.Templates.cart(BestPracticeHelper.getSortedBpMap(bpService.findByIds(ids)), new BestPracticeHelper()).render();

    }

    @CheckedTemplate
    public static class Templates {
        public static native TemplateInstance cart(Map<FamilyEnum, List<BestPractice>> bps, BestPracticeHelper helper);
    }


}
