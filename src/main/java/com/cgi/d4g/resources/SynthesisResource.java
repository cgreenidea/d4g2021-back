package com.cgi.d4g.resources;

import com.cgi.d4g.bo.LifecycleBpsResult;
import com.cgi.d4g.service.BestPracticeHelper;
import com.cgi.d4g.service.BestPracticeService;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;
import io.quarkus.qute.CheckedTemplate;
import io.quarkus.qute.TemplateInstance;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

@Path("synthesis")
public class SynthesisResource {

    @Inject
    BestPracticeService bpService;

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public String get(@FormParam("checked") List<String> ids) {
        return SynthesisResource.Templates.synthesis(BestPracticeHelper.getReportBpMap(bpService.findByIds(ids)), new BestPracticeHelper()).render();

    }

    @Path("/downloadPdf")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response downloadReport(@FormParam("checked") List<String> ids) {
        String dt = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.FRANCE).format(LocalDateTime.now());

        try {
            File tmp = File.createTempFile("synthesis_report_"+ dt , ".pdf");
            OutputStream os = new FileOutputStream(tmp);
            PdfRendererBuilder builder = new PdfRendererBuilder();
            builder.useFastMode();
            builder.withHtmlContent(SynthesisResource.Templates.synthesis(BestPracticeHelper.getReportBpMap(bpService.findByIds(ids)), new BestPracticeHelper()).render(),"");
            builder.toStream(os);
            builder.run();
            return Response.ok(new FileInputStream(tmp).readAllBytes())
                .type(MediaType.APPLICATION_OCTET_STREAM)
                .header("Content-Disposition", "attachment;filename=" + tmp.getName())
                .build();
        }catch(IOException  ioException){
            Response.ResponseBuilder response = Response.serverError();
            return response.build();
        }

    }


    @CheckedTemplate
    public static class Templates {
        private Templates() {
        }
        public static native TemplateInstance synthesis(List<LifecycleBpsResult> reportBps, BestPracticeHelper helper);

    }

}
