var coll = document.getElementsByClassName("collapsible");
var i;
for (i = 0; i < coll.length; i += 1) {
    coll[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.maxHeight) {
            content.style.maxHeight = null
        } else {
            content.style.maxHeight = content.scrollHeight + "px"
        }
    })
}

var collSyn = document.getElementsByClassName("box-title");

for (i = 0; i < collSyn.length; i += 1) {

    collSyn[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var content = this.parentElement.childNodes[3];

        if (content.style.display !== "none" && content.style.display !== "") {
            content.style.display = "none";
        } else {
            content.style.display = "inherit";
        }
    })
}

var modal = document.getElementById("myModal");
var btn = document.getElementsByClassName("btn-modal");
var span = document.getElementsByClassName("close")[0];
for (i = 0; i < btn.length; i += 1) {
    btn[i].onclick = function (event) {
        var parsedId = event.target.id.split(":")[1];
        populatePopup(parsedId);
        modal.style.display = "block";
    };
}
if(span !== undefined){
    span.onclick = function () {
        modal.style.display = "none"
    };
}
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none"
    }
};

function populatePopup(id){
    document.getElementById("myModal-critere").innerText = "Critère : "+document.getElementById("btnmodal-critere-"+id).value;
    document.getElementById("myModal-justification").innerText = "Justification : "+document.getElementById("btnmodal-justification-"+id).value;
    document.getElementById("myModal-lifecycle").innerText = "Etape du cycle de vie : "+document.getElementById("btnmodal-lifecycle-"+id).value;
    document.getElementById("myModal-kpi").innerHTML = "Indicateur : "+(document.getElementById("btnmodal-kpi-"+id).value==='' ? '<i>Non applicable</i>':document.getElementById("btnmodal-kpi-"+id).value);
}
let selected_element = document.querySelectorAll('.bp-checkbox');
      console.log(selected_element);
      let categories = document.querySelectorAll('.bp-category span.category-name');
      console.log(categories);
      for(let element of selected_element) {
          element.addEventListener('change', function(e) {
            if(element.checked == true) {
              for(let cat of categories) {
                  if(cat.classList[2] == element.classList[1]) {
                  let span_wrapper = cat.getElementsByTagName("span");
                  let span_increment = span_wrapper[0];
                    span_increment.innerText = parseInt(span_increment.innerText) + 1;
                  }
              }
            }
            if(element.checked == false) {
              for(let cat of categories) {
                  if(cat.classList[2] == element.classList[1]) {
                   let span_wrapper = cat.getElementsByTagName("span");
                   let span_increment = span_wrapper[0];
                    if(span_increment.innerText != 0) {
                      span_increment.innerText = parseInt(span_increment.innerText) - 1;
                    }
                  }
              }
            }
          });
      }
